package com.xudongbase.java;

import com.xudongbase.common.util.MatchUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MatchTest {
    /**
     * 测试匹配中文
     */
    @Test
    public void testMatchChinese() {
        System.out.println(MatchUtil.matchChineseCount("0页"));
        System.out.println(MatchUtil.matchChineseCount("旭东怪"));
        System.out.println(MatchUtil.matchChineseCount("0page"));
    }

}
