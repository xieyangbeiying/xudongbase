package com.xudongbase.java;

import com.google.common.primitives.Bytes;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class ConvertTest {

    @Test
    public void testBytesToList() {

        byte[] bytes = new byte[1];
        //byte数组转List第一种方法
        List<Byte> byteList1 = Bytes.asList(bytes);
        //byte数组转List第二种方法
        List<Byte> byteList2 = new ArrayList<>(Bytes.asList(bytes));
    }
}
