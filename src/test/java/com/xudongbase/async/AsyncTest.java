package com.xudongbase.async;

import com.xudongbase.test.service.AsyncService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

@SpringBootTest
public class AsyncTest {
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private AsyncService asyncService;
    /**
     * 测试多线程
     */
    @Test
    public void testAsync(){
        for (int i = 0; i < 100; i++) {
            System.out.print("\ti=" + i);
        }
        for (int j = 0; j < 100; j++) {
            asyncService.test(j);
        }
        System.out.print("i=" + 1111);
    }

}
