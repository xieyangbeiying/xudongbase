package com.xudongbase.json;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootTest
public class JsonSerializeTest {
    @Autowired
    private ObjectMapper objectMapper;

    /**
     * 测试fastjson，long、Date、LocalDateTime类型序列化
     */
    @Test
    public void testFastjsonSerialize() {
        List<Object> list = new ArrayList<>();
        list.add(1440931124753108994L);
        list.add(new Date());
        list.add(LocalDate.now());
        list.add(LocalDateTime.now());
        System.out.println(JSONObject.toJSONString(list));
    }

    /**
     * 测试jackson，long、Date、LocalDateTime类型序列化
     */
    @Test
    public void testJacksonSerialize() throws Exception {
        List<Object> list = new ArrayList<>();
        list.add(1440931124753108994L);
        list.add(new Date());
        list.add(LocalDate.now());
        list.add(LocalDateTime.now());
        System.out.println(objectMapper.writeValueAsString(list));
    }

}
