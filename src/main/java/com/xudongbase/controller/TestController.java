package com.xudongbase.controller;

import com.xudongbase.common.model.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @GetMapping("/test")
    public Result test() {
        int i = 1 / 0;
        return Result.success("操作成功");
    }
}
