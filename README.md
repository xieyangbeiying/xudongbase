# xudongbase

# 1 介绍
主要是项目中可以用到的共通方法。

# 2 master分支

## 2.1 工具类

### FileUtil（文件工具类）
1.  createNewFile：创建新文件，文件不存在时创建，文件存在时自动结束
### EqualsUtil（相等判断工具类）
1.  equals：支持Integer类型相等判断
### ScopeUtil（范围判断工具类）
1.  isBetween：是否介于范围内判断，支持int、LocalDate类型
2.  isIntersection：两个范围是否有交集，支持int、LocalDate类型
### ConvertUtil（转换工具类）
1.  arrayToList：数组转List，支持byte、Byte、Integer、Long、Float、Double类型
### MatchUtil（匹配工具类）
1.  isMatchChinese：是否匹配中文字符，支持String、char类型
2.  matchChineseCount：匹配的中文字符数量
## 2.2 全局配置

### JsonConfig（全局序列化配置）
1.  支持jackson和fastjson序列化工具包
2.  支持long、Long、Date、DateTime类型
### AsyncConfig（多线程配置）

# 3 软件架构
Hutool、lombox、EasyExcel、POI、fastjson、jackson、MinIO

# 4 参与贡献
1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

# 5 个人博客
https://blog.csdn.net/qq_38974638

# 6 QQ交流群
644094294

# 7 子分支

## 7.1 minio
该分支主要是minio相关的操作方法（上传、下载、删除）
## 7.2 wechat
该分支主要是企业微信的相关操作（发送通知，获取信息）
## 7.3 poi
该分支主要是poi相关的操作（Word、Excel）
### 7.3.1 easyexcel
该分支主要excel相关的操作（批量设置样式、批量添加批注、批量合并单元格、冻结行和列、设置行高列宽，隐藏行和列、绑定下拉框数据）
## 7.4 minio_poi
该分支主要是将poi生成的文件上传至minio的操作方法(Excel文件、Word文件)
## 7.5 design_pattern
该分支主要是介绍二十三种设计模式
